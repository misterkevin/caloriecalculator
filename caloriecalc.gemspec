
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "caloriecalc/version"

Gem::Specification.new do |spec|
  spec.name          = "caloriecalc"
  spec.version       = Caloriecalc::VERSION
  spec.authors       = ["kevin.deady@gmail.com"]
  spec.email         = ["kevin.deady@gmail.com"]

  spec.summary       = %q{A Gem to calculate the caories needed for a person for a day.}
  spec.description   = %q{Functions avaiable: 
    WHO, Harris Benedict and MifflinStJeor will return calore reqs 
    for a person based on the inputs age, height, weight, sex based on their respective algorithms.
    Function CalorieCalculation returns a hash containing the values for each algorithm. 
    Function CaloriesBurnedPerMinute takes the weight and MET and returns the caloie burn per minute for a person performing that exercise}
  spec.homepage      = ""
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org/"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "There is no URL."
    spec.metadata["changelog_uri"] = "This should be the CHANGELOG.md URL here."
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
end
